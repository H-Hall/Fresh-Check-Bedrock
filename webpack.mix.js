const mix = require('laravel-mix').mix;
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');

const resources = 'assets-src';
const themePath = 'web/app/themes/freshcheck';
const assetsPath = 'web/app/themes/freshcheck/assets';
const devSite = 'freshcheck.dev';

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

imagemin([`${resources}/images/*.{jpg,png,svg,gif}`], `${assetsPath}/images`, {
  plugins: [
    imageminJpegtran(),
    imageminPngquant({quality: '65-80'})
  ]
}).then(files => {
  console.log(files);
  //=> [{data: <Buffer 89 50 4e …>, path: 'build/images/foo.jpg'}, …]
});

imagemin([`${resources}/images/icons/*.{jpg,png,svg,gif}`], `${assetsPath}/images/icons`, {
  plugins: [
    imageminJpegtran(),
    imageminPngquant({quality: '65-80'})
  ]
}).then(files => {
  console.log(files);
  //=> [{data: <Buffer 89 50 4e …>, path: 'build/images/foo.jpg'}, …]
});


mix.setPublicPath(assetsPath);
mix.setResourceRoot('../');

mix.scripts([
        //`node_modules/jquery/dist/jquery.min.js`,
        `node_modules/jquery-form/dist/jquery.form.min.js`,
        `node_modules/mobius1-selectr/dist/selectr.min.js`,
        `node_modules/slick-carousel/slick/slick.min.js`,
        `node_modules/magnific-popup/dist/jquery.magnific-popup.min.js`,
        `web/app/wp/wp-includes/js/wp-embed.js`,
        `${resources}/js/app.js`
      ], `${assetsPath}/js/app.js`);

mix.sass(`${resources}/scss/app.scss`, `${assetsPath}/css/app.css`)
   .options({
            processCssUrls: false
           });

mix.browserSync({
    proxy: devSite,
    files: [
        `${themePath}/**/*.php`,
        `${themePath}/**/*.twig`,
        `${assetsPath}/**/*.js`,
        `${assetsPath}/**/*.css`
    ]
});

// Hash and version files in production.
if (mix.config.inProduction) {
    mix.version();
}
