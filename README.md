# Impression Boilerplate
## [Bedrock](https://github.com/roots/bedrock)
## [Timber](https://github.com/timber/timber)
## [Laravel Mix](https://dotdev.co/laravel-mix-wordpress/)

## Requirements

* PHP >= 7.0
* Composer - [Install](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
* Node + NPM

## Installation

1. Create a new project - git clone (this git repo)
2. Copy `.env.example` to `.env` and update environment variables:
  * `DB_NAME` - Database name
  * `DB_USER` - Database user
  * `DB_PASSWORD` - Database password
  * `DB_HOST` - Database host
  * `WP_ENV` - Set to environment (`development`, `staging`, `production`)
  * `WP_HOME` - Full URL to WordPress home (http://example.com)
  * `WP_SITEURL` - Full URL to WordPress including subdirectory (http://example.com/wp)
  * `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT` - Generate with [wp-cli-dotenv-command](https://github.com/aaemnnosttv/wp-cli-dotenv-command) or from the [Roots WordPress Salt Generator](https://roots.io/salts.html)
  * `THEME_NAME` - Name of the theme folder
  * `ACF_PRO_KEY` - ACF Pro key so that the ACF Pro plugin can be installed via composer
  * `ILAB_AWS_S3_ACCESS_KEY` - (Optional) AWS IAM account with S3 bucket access, Key [AWS IAM](https://console.aws.amazon.com/iam/home?region=eu-west-1#)
  * `ILAB_AWS_S3_ACCESS_SECRET` - (Optional) AWS IAM account with S3 bucket access, Secret [AWS IAM](https://console.aws.amazon.com/iam/home?region=eu-west-1#)
  * `ILAB_AWS_S3_BUCKET` - (Optional) Bucket setup for this specific site in [AWS S3](https://s3.console.aws.amazon.com/s3/home?region=eu-west-1#)
  * `GMAPS_API_KEY` - (Optional) Used for ACF maps in the admin area, create Maps JS API key [Google Console](https://console.developers.google.com)
  * `MAILCHIMP_API_KEY` - (Optional) Generated within mailchimp
  * `MAILCHIMP_LIST_ID` - (Optional Generated within mailchimp
3. Run `composer install`
4. Rename the theme directory  web/app/themes/theme_name
5. Rename paths in webpack.mix.js to point to renamed theme directory
6. Install npm modules `npm install`
7. Run webpack `npm run dev`
8. Access WP admin at `http://example.com/wp/wp-admin`

## Theme Development

Uses Webpack + Laravel mix for compile CSS and JS from the root assets folder to the theme assets folder

`npm run dev` - To compile files for dev (No versioning)
`npm run watch` - Runs dev with Browser Sync
`npm run prod` - Runs versioning


## Deploys

`composer install` must be run as part of the deploy process.
`npm run prod` - run on deploy to compile and add versioning
# Fresh-Check-Bedrock
