<?php
// class Quote_Widget extends WP_Widget {
//
// 	public function __construct() {
// 		$widget_ops = array(
// 			'classname' => 'quote_widget',
// 			'description' => 'Display a quote box in the sidebar',
// 		);
// 		parent::__construct( 'quote_widget', 'Quote Widget - Elite', $widget_ops );
// 	}
//
// 	public function widget( $args, $instance ) {
// 		echo '<div class="col-xs-12 text-align--left m-bottom--xxl side-quote side">
//               <blockquote class="m-left--zero m-top--zero m-bottom--zero">
//                   <p><i>'.$instance['quote'].'</i></p>
//               </blockquote>
//           </div>';
// 	}
//
// 	public function form( $instance ) {
// 		$quote = !empty($instance['quote']) ? $instance['quote'] : 'Add Quote Here';
// 			echo '<p>
// 			<label for="'.$this->get_field_id( 'quote' ).'">'. __( 'Quote:', 'elite' ) .'</label>
// 			<input class="widefat" id="'. $this->get_field_id( 'quote' ) .'" name="'. $this->get_field_name( 'quote' ) .'" type="text" value="'.$quote.'">
// 			</p>';
// 	}
//
// 	public function update( $new_instance, $old_instance ) {
// 		$instance = array();
// 		$instance['quote'] = ( ! empty( $new_instance['quote'] ) ) ? strip_tags( $new_instance['quote'] ) : '';
// 		return $instance;
// 	}
// }
//
// class Text_Widget extends WP_Widget {
//
// 	public function __construct() {
// 		$widget_ops = array(
// 			'classname' => 'text_widget',
// 			'description' => 'Display Elite styled box in the sidebar',
// 		);
// 		parent::__construct( 'text_widget', 'Text Widget - Elite', $widget_ops );
// 	}
//
// 	public function widget( $args, $instance ) {
// 		echo '<div class="col-xs-12 text-align--left sidebar m-bottom--xxl">';
// 		if($instance['title'])
// 			echo '<h4 class="m-bottom--l m-top--zero">'.$instance['title'].'</h4>';
// 		if($instance['text'])
// 			echo '<p>'.$instance['text'].'</p>';
// 		if($instance['button_text'] && $instance['button_link'])
// 			echo '<a href="'.$instance['button_link'].'" class="btn btn--small d--table float--l m-top--xl">'.$instance['button_text'].'</a>';
// 		echo '</div>';
// 	}
//
// 	public function form( $instance ) {
//     $title = !empty($instance['title']) ? $instance['title'] : 'Title goes here';
//     $text = !empty($instance['text']) ? $instance['text'] : 'text goes here';
//     $button_text = !empty($instance['button_text']) ? $instance['button_text'] : 'button text goes here';
//     $button_link = !empty($instance['button_link']) ? $instance['button_link'] : 'button link goes here';
//
// 			echo '<p>
// 						<label for="'.$this->get_field_id( 'title' ).'">'. __( 'Title:', 'elite' ) .'</label>
// 						<input class="widefat" id="'. $this->get_field_id( 'title' ) .'" name="'. $this->get_field_name( 'title' ) .'" type="text" value="'.$title.'">
// 						</p>
// 						<p>
// 							<label for="'.$this->get_field_id( 'text' ).'">'. __( 'Text:', 'elite' ) .'</label>
// 							<textarea class="widefat" id="'. $this->get_field_id( 'text' ) .'" name="'. $this->get_field_name( 'text' ) .'">'.$text.'</textarea>
// 						</p>
// 						<p>
// 							<label for="'.$this->get_field_id( 'button_text' ).'">'. __( 'Button Text:', 'elite' ) .'</label>
// 							<input class="widefat" id="'. $this->get_field_id( 'button_text' ) .'" name="'. $this->get_field_name( 'button_text' ) .'" type="button_text" value="'.$button_text.'">
// 						</p>
// 						<p>
// 							<label for="'.$this->get_field_id( 'button_link' ).'">'. __( 'Button Link:', 'elite' ) .'</label>
// 							<input class="widefat" id="'. $this->get_field_id( 'button_link' ) .'" name="'. $this->get_field_name( 'button_link' ) .'" type="button_link" value="'.$button_link.'">
// 						</p>
// 						';
//
// 	}
//
// 	public function update( $new_instance, $old_instance ) {
// 		$instance = array();
// 		$instance['title'] = (!empty($new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
// 		$instance['text'] = (!empty($new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
// 		$instance['button_text'] = (!empty($new_instance['button_text'] ) ) ? strip_tags( $new_instance['button_text'] ) : '';
// 		$instance['button_link'] = (!empty($new_instance['button_link'] ) ) ? strip_tags( $new_instance['button_link'] ) : '';
// 		return $instance;
// 	}
// }
//  
