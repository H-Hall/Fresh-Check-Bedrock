<?php

require 'assets/php/helpers.php';
require 'assets/php/widgets.php';

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});

	add_filter('template_include', function($template) {
		die("Please install and activate the Timber plugin to continue. ");
	});

	return;
}

Timber::$dirname = array('views');

class ImpressionSite extends TimberSite {

	function __construct() {
		/**
		 * Add filters
		 */
		add_action('acf/init', array($this, 'add_options_page'));
		add_action('init', array( $this, 'register_post_types' ));
		add_action('init', array( $this, 'register_taxonomies' ) );
		add_action('wp_enqueue_scripts', array($this, 'enqueue_styles_scripts'));
		add_action('widgets_init', array($this, 'unregister_default_widgets'), 11);
		add_action('widgets_init', array($this, 'register_sidebars'));
		add_action('widgets_init', array($this, 'register_widgets'));
		add_action('after_setup_theme', array($this, 'register_menus'));
		add_action('wp_dashboard_setup', array($this, 'remove_dashboard_widgets'));
		add_action('init', array($this, 'impression_cleanup'));
		add_action('wp_head', array($this, 'impression_remove_recent_comments_style'), 1 );
		add_action('rest_api_init', array($this, 'rest_endpoint_newslleter_subscription'));
		add_action('wp_before_admin_bar_render', array($this, 'impression_remove_toolbar_items'), 0);
		add_filter('timber_context', array( $this, 'add_to_context' ));
		add_filter('get_twig', array( $this, 'add_to_twig' ));
		add_filter('acf/init', array( $this, 'acf_setup'));
		add_filter('style_loader_src', array($this, 'remove_wp_ver_css_js'), 9999 );
		add_filter('script_loader_src', array($this, 'remove_wp_ver_css_js'), 9999 );
		add_filter('wp_head', array( $this, 'impression_remove_wp_widget_recent_comments_style'), 1 );
        add_filter('gallery_style', array( $this, 'impression_gallery_style'));
		add_filter('the_content', array( $this, 'impression_filter_ptags_on_images'));
		add_filter('wpcf7_load_js', '__return_false');
		add_filter('wpcf7_load_css', '__return_false');
		add_filter('template_include', array($this, 'load_twig_templates'), 99);
		add_filter('acf/fields/flexible_content/layout_title', array($this, 'flexi_title_override'), 10, 4);
		add_filter( 'wpseo_metabox_prio', function() { return 'low';});
		add_filter( 'wpseo_stylesheet_url', '__return_empty_string', 10, 1 );
		add_filter('wpseo_breadcrumb_separator', array($this, 'breadcrumbs_sep'));
        add_filter('embed_oembed_html', array($this, 'vnmFunctionality_embedWrapper'), 10, 4);
       // add_action('woocommerce_after_shop_loop_item',array($this , 'replace_add_to_cart'));
        add_filter('woocommerce_add_error', array($this, 'add_max_notice'));
        add_filter( 'wp_calculate_image_srcset_meta', '__return_empty_array' );
        add_filter( 'single_product_archive_thumbnail_size', function($size){ return 'image-block'; });

		/**
		 * Theme setup
		 */
		add_image_size('header-logo', 200, 100);
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_theme_support( 'yoast-seo-breadcrumbs');
		add_theme_support( 'title-tag' );
        add_theme_support( 'woocommerce' );
		add_shortcode('button', array($this, 'button_shortcode'));
		add_shortcode('fa', array($this, 'fa_shortcode'));
		add_image_size('banner', 1920, 600, true);
		add_image_size('video-thumb', 600, 340, true);
		add_image_size('three-block', 360, 220);
		add_image_size('image-block', 360, 360, true);
		add_image_size('mention', 999, 150);

		parent::__construct();
	}

	function add_max_notice($message){
	    if(strpos($message, 'The maximum allowed quantity') !== false){
	        $message = str_replace('Please remove some items from your cart.', '', $message);
	        $message .= ' To order more than 5 please <a target="_blank" style="text-decoration: underline;" href="/contact-us/">contact us directly</a>';
        }
        return $message;
    }

    function replace_add_to_cart() {
        global $product;
        $link = $product->get_permalink();
        echo do_shortcode('[button link="' . esc_attr($link) . '" text="More Information"]More Information[/button]');
    }

    function vnmFunctionality_embedWrapper($html, $url, $attr, $post_id) {
        return '<div class="videoWrapper">' . $html . '</div>';
    }

	function breadcrumbs_sep(){
		return '</li><li>';
	}

	// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
	function impression_filter_ptags_on_images($content){
	   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	}

	function impression_remove_wp_widget_recent_comments_style() {
	   if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
	      remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	   }
	}

	function impression_remove_toolbar_items(){
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('wp-logo');
	}

	function impression_remove_recent_comments_style() {
	  global $wp_widget_factory;
	  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
	    remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	  }
	}

	function impression_gallery_style($css) {
	  return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
	}


	function impression_cleanup() {
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'index_rel_link' );
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	  remove_action( 'wp_head', 'wp_generator' );
	  remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	  register_taxonomy( 'post_tag', array() ); //Unset tags taxonomy


        // Woo
        remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
        remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
    }

	function remove_dashboard_widgets() {
		global $wp_meta_boxes;

		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	}

	function unregister_default_widgets() {
		unregister_widget('WP_Widget_Pages');
		unregister_widget('WP_Widget_Calendar');
		unregister_widget('WP_Widget_Archives');
		unregister_widget('WP_Widget_Links');
		unregister_widget('WP_Widget_Meta');
		// unregister_widget('WP_Widget_Search');
		// unregister_widget('WP_Widget_Text');
		unregister_widget('WP_Widget_Categories');
		// unregister_widget('WP_Widget_Recent_Posts');
		unregister_widget('WP_Widget_Recent_Comments');
		unregister_widget('WP_Widget_RSS');
		unregister_widget('WP_Widget_Tag_Cloud');
		// unregister_widget('WP_Nav_Menu_Widget');
		unregister_widget('Twenty_Eleven_Ephemera_Widget');
	}

	function register_post_types() {
		// $args = array(
    //   'public' => true,
    //   'label'  => 'Jobs',
		// 	'menu_icon' => 'dashicons-businessman',
		// 	'show_ui' => true
    // );
    // register_post_type( 'jobs', $args );
	}

	function register_sidebars() {
		// register_sidebar([
		// 	'name'          => 'Posts Sidebar',
		// 	'id'            => 'posts-sidebar',
		// 	'description'   => '',
		//   'class'         => '',
		// 	'before_widget' => '<li id="%1$s" class="widget %2$s">',
		// 	'after_widget'  => '</li>',
		// 	'before_title'  => '<h2 class="widgettitle">',
		// 	'after_title'   => '</h2>'
		// ]);
	}

	function register_widgets(){
		// register_widget('Quote_Widget');
		// register_widget('Text_Widget');
	}

	function register_menus(){
		register_nav_menus( array(
			'main' => 'Main Menu',
			'footer' => 'Footer Navigation'
		));
	}

	function register_taxonomies() {
		// Register taxonomies here.
		// https://codex.wordpress.org/Function_Reference/register_taxonomy
	}

	function enqueue_styles_scripts(){
		if (!is_admin()) {
			//wp_deregister_script('jquery');
			wp_deregister_script('wp-embed');

	    wp_enqueue_style('css', mix('/css/app.css'));
	    wp_enqueue_script('js', mix('/js/app.js'), array('jquery'), '', true);

			wp_localize_script( 'js', 'wpcf7', [
				'apiSettings' => array(
						'root' => esc_url_raw( get_rest_url() ),
						'namespace' => 'contact-form-7/v1',
					),
					'recaptcha' => array(
						'messages' => array(
							'empty' => __( 'Please verify that you are not a robot.', 'contact-form-7' ),
					),
					),
					'cached' => 1
			]);


		}
	}

	function remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) ) {
      $src = remove_query_arg( 'ver', $src );
		}
    return $src;
	}

	function acf_setup() {
		if(defined('GMAPS_API_KEY'))
			acf_update_setting('google_api_key', GMAPS_API_KEY);
	}

	function add_to_context( $context ) {
		$context['menu'] = new TimberMenu();
		$context['site'] = $this;
		$context['options'] = get_fields('options');
		if(isset($_GET) && !empty($_GET)){
			foreach ($_GET as $key => $value) {
				$context['get'][$key] = $value;
			}
		}
		return $context;
	}

	/**
	 * Reduce the reliance of numerous thin WordPress template files just to load twig templates.
	 * @param  string $template template file URL
	 * @return string $template or null when Timber is rendered
	 */
	function load_twig_templates( $template ) {
		global $wp_query;

		$context = Timber::get_context();

        if( is_woocommerce() ){
            $context['sidebar'] = Timber::get_widgets( 'shop-sidebar' );

            if ( is_singular( 'product' ) ) {
                $context['post']    = Timber::get_post();
                $product            = wc_get_product( $context['post']->ID );
                $context['product'] = $product;

                // Get related products
                $related_limit               = wc_get_loop_prop( 'columns' );
                $related_ids                 = wc_get_related_products( $context['post']->id, $related_limit );
                $context['related_products'] =  Timber::get_posts( $related_ids );

                // Restore the context and loop back to the main query loop.
                wp_reset_postdata();

                Timber::render( 'views/woo/single-product.twig', $context );
            } else {
                if($pageid = get_option('wppageoverrides_product_id')){
                    $context['post'] = new Timber\Post($pageid);
                }
                $posts = Timber::get_posts();
                $context['products'] = $posts;
                $context['categories'] = array_reverse(get_terms('product_cat'));

                foreach($context['categories'] as &$category){
                    $category->products = Timber::get_posts(array(
                        'post_type' => 'product',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_cat',
                                'field' => 'slug',
                                'terms' => $category->slug
                            )
                        )
                        ));
                }

                if ( is_product_category() ) {
                    $queried_object = get_queried_object();
                    $term_id = $queried_object->term_id;
                    $context['category'] = get_term( $term_id, 'product_cat' );
                    $context['title'] = single_term_title( '', false );
                }

                Timber::render( 'views/woo/archive.twig', $context );
            }
            return;
        }

		if ( is_404() ) {
			if($pageid = get_option('wppageoverrides_404_id')){
				$context['post'] = new Timber\Post($pageid);
			}
			Timber::render( '404.twig', $context );
			return;
		}

		if ( is_search() ) {
			if($pageid = get_option('wppageoverrides_search_id')){
				$context['post'] = new TimberPost($pageid);
			}
			$templates = array( 'search.twig', 'archive.twig', 'index.twig' );
			$context['title'] = 'Search results for <u>'. get_search_query() .'</u>';
			$context['posts'] = Timber::get_posts(array('s' => get_search_query()));
			$context['pagination'] = Timber::get_pagination();

			Timber::render( $templates, $context );
			return;
		}

		if ( is_front_page() ) {
			$context['post'] = new Timber\Post();
			Timber::render( 'home.twig', $context );
			return;
		}

		if ( is_home() ){
			$context['post'] = new Timber\Post(get_option( 'page_for_posts' ));
			$context['posts'] = Timber::get_posts();
			$context['category'] = Timber::get_terms( 'category' );
			Timber::render( 'index.twig', $context );
			return;
		}

		if ( is_author() ) {
			$context['posts'] = Timber::get_posts();
			if ( isset( $wp_query->query_vars['author'] ) ) {
				$author = new TimberUser( $wp_query->query_vars['author'] );
				$context['author'] = $author;
				$context['title'] = 'Author Archives: ' . $author->name();
			}
			Timber::render( array( 'author.twig', 'archive.twig' ), $context );
			return;
		}

		if ( is_single() ) {
			$context['post'] = new Timber\Post();

			if ( post_password_required( $context['post']->ID ) ) {
				Timber::render( 'single-password.twig', $context );
			} else {
				Timber::render( array( 'single-' . $context['post']->ID . '.twig', 'single-' . $context['post']->post_type . '.twig', 'single.twig' ), $context );
			}
			return;
		}

		if ( is_page() ) {
			$context['post'] = Timber::query_post();

			if ( post_password_required( $context['post']->ID ) ) {
				Timber::render( 'single-password.twig', $context );
			} else {
				Timber::render( array('page.twig' ), $context );
			}
			return;
		}

		if ( is_archive() ) {
			$templates = array( 'archive.twig', 'index.twig' );

			if($pageid = get_option('wppageoverrides_'.get_post_type().'_id')){
				$context['post'] = new Timber\Post($pageid);
			}

			$context['title'] = 'Archive';
			if ( is_day() ) {
				$context['title'] = 'Archive: '.get_the_date( 'D M Y' );
			} else if ( is_month() ) {
				$context['title'] = 'Archive: '.get_the_date( 'M Y' );
			} else if ( is_year() ) {
				$context['title'] = 'Archive: '.get_the_date( 'Y' );
			} else if ( is_tag() ) {
				$context['title'] = single_tag_title( '', false );
			} else if ( is_category() ) {
				$context['title'] = single_cat_title( '', false );
				array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
			} else if ( is_post_type_archive() ) {
				$context['title'] = post_type_archive_title( '', false );
				array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
			}

			$context['current_cat'] = new TimberTerm();

			switch( get_post_type() ){
				// case 'jobs':
				// 	$context['locations'] = Timber::get_terms( 'location' );
				// break;
				default:
					$context['category'] = Timber::get_terms( 'category' );
				break;
			}
			Timber::render( $templates, $context );
			return;
		}

		return $template;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		//$twig->addFilter('number_to_word', new Twig_SimpleFilter('number_to_word', array($this, 'number_to_word')));
		return $twig;
	}

	function add_options_page() {
		acf_add_options_page( [
			'page_title' 	=> 'Site Settings',
			'menu_title' 	=> 'Site Settings',
			'menu_slug' 	=> 'site-settings',
			'capability' 	=> 'edit_posts',
			'icon_url'		=> 'dashicons-screenoptions',
			'position'		=> '20.346456745'
		] );
	}

	function button_shortcode( $atts ) {
		$a = shortcode_atts( array(
			'link' => '#linkhere',
			'text' => 'Button Text',
			'class' => 'btn btn--primary m-top--l m--bottom-l',
			'extraclass' => '',
			'style' => ''
		), $atts );

		return "<a href=\"{$a['link']}\" class=\"{$a['class']} {$a['extraclass']}\" style=\"{$a['style']}\">{$a['text']}</a>";
  }

	function rest_endpoint_newslleter_subscription() {
		if (MAILCHIMP_API_KEY !== null && MAILCHIMP_LIST_ID !== null) {
			$mailchimp = [
				'methods' => 'GET',
				'callback' => array($this, 'mailchimp_signup')
			];
			register_rest_route( 'mailchimp/v1', '/email/(?P<email>\S+)/name/(?P<name>\S+)', $mailchimp);
		}
	}

	function mailchimp_signup(WP_REST_REQUEST $request){
		$email = $param = $request->get_param( 'email' );
		$name = $param = $request->get_param( 'name' );
		$apikey = MAILCHIMP_API_KEY;
		$listid = MAILCHIMP_LIST_ID;
		$mailchimp = new \DrewM\MailChimp\MailChimp($apikey);
		$res = $mailchimp->post("lists/$listid/members", [
			'email_address' => $email,
			'status' => 'subscribed',
			'merge_fields' => ['FNAME' => $name]
		]);
		return $res;
	}

	/**
	 * This filter allows you to modify the text (HTML) displayed at the top of each flexible content layout
	 * @param  $title (string) the layout title text. Defaults to the layout title
	 * @param  $field (array) the flexible content field settings array
	 * @param  $layout (array) the current layout settings array
	 * @param  $i (int) the current layout index. The first layout index is 0
	 * @return string text (HTML to be displayed at the top of flexible content layout)
	 * https://www.advancedcustomfields.com/resources/acf-fields-flexible_content-layout_title/
	 */
	function flexi_title_override($title, $field, $layout, $i){
		if($t = get_sub_field('title'))
			$title = '<strong>' . $t . '</strong> | ' . $title;
		return $title;
	}

	function fa_shortcode( $atts ) {
		$a = shortcode_atts( array(
			'icon' => 'address-book',
		), $atts );

		return "<span class=\"fa-shortcode-wrapper\"><i class=\"fa fa-{$a['icon']}\" aria-hidden=\"true\"></i></span>";
	}



}

new ImpressionSite();


function timber_set_product( $post ) {
    global $product;

    if ( is_woocommerce() ) {
        $product = wc_get_product( $post->ID );
    }
}