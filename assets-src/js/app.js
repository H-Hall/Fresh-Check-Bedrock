jQuery(window).on('scroll', function($) {
    var scrollTop = jQuery(this).scrollTop();
    jQuery('.to-fade-in').each(function() {
      var topDistance = jQuery(this).offset().top;
      if ( (topDistance-800) < scrollTop ) {
        jQuery(this).addClass('fade-in');
      }
    });

    var scroll = jQuery(window).scrollTop();

    // if (scroll >= 100) {
    //     jQuery(".header").addClass("header--reduce");
    // } else {
    //     jQuery(".header").removeClass("header--reduce");
    // }

});

jQuery(document).ready(function($){

    //mobile nav switch
    $('#nav-icon').click(function(){
        $(this).toggleClass('open');
        $('.main-nav__navigation').toggleClass('main-nav__navigation--active');
    });

    //open sub nav
    $('.nav--plus').click(function(){
        if ($(this ).hasClass( "open-nav" )){
            $(this).parent().find('.mobile-nav').toggleClass('mobile-nav--active');
        }
    });

    //mobile hide classes
    function screenClass() {
        if($(window).innerWidth() > 768) {
            $('.main-nav__navigation').removeClass('main-nav__navigation--active');
            $('#nav-icon').removeClass('open');
            $('.nav--plus').removeClass('open-nav');
            $('.mobile-nav').removeClass('mobile-nav--active');
        }else{
            $('.nav--plus').addClass('open-nav');
        }
    }

    // Fire.
    screenClass();

    // And recheck if window gets resized.
    $(window).bind('resize',function(){
        screenClass();
    });

    //header search

    $( '#search' ).click( function( e ) {
        console.log('SEARCH');
        $('.utility-nav__search-form').toggleClass( "utility-nav__search-form--active" );
    });

    $('.utility-nav--search').on('input', function() {
        $(this).removeClass('error');
    });

    //testimonial slider

    if ( $('.testimonial__slider').length > 0 ) {

            $('.testimonial__slider').slick({
                autoplay: false,
                autoplaySpeed: 5000,
                speed: 1000,
                arrows: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                useTransform: true,
                cssEase: 'ease-in-out',
                dots: true,
            });

    }

    if ( $('.mentions__slider').length > 0 ) {

            $('.mentions__slider').slick({
                autoplay: true,
                autoplaySpeed: 3000,
                speed: 1000,
                arrows: false,
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                useTransform: true,
                cssEase: 'ease-in-out',
                dots: false,
                responsive: [
                            {
                              breakpoint: 800,
                              settings: {
                                slidesToShow: 4,
                              }
                            },
                            {
                              breakpoint: 600,
                              settings: {
                                slidesToShow: 2,
                              }
                            }

                          ]
            });

    }

    //scroll to builder

    $(".scrollto").click(function() {

        var to = $(this).attr('name');

        $('html, body').animate({
            scrollTop: $(to).offset().top - 200
        }, 2000);

    });

    $(".main-nav__navigation li i").click(function() {

        console.log('click li i')
        $(this).parent().find('ul').toggleClass('active');

        event.stopPropagation();

    });


    //faqs

    if( jQuery(".toggle .toggle__toggle-title").hasClass('active') ){
        jQuery(".toggle .toggle__toggle-title.active").closest('.toggle').find('.toggle__toggle-inner').show();
    }
    jQuery(".toggle .toggle__toggle-title").click(function(){
        if( jQuery(this).hasClass('active') ){
            jQuery(this).removeClass("active").closest('.toggle').find('.toggle__toggle-inner').slideUp(200);
        }
        else{   jQuery(this).addClass("active").closest('.toggle').find('.toggle__toggle-inner').slideDown(200);
        }
    });

    //Video popup

    // $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
    //     disableOn: 700,
    //     type: 'iframe',
    //     mainClass: 'mfp-fade',
    //     removalDelay: 160,
    //     preloader: false,
    //     fixedContentPos: false
    // });
    //
    // //image popup
    //
    // $('.image-popup-vertical-fit').magnificPopup({
    //     type: 'image',
    //     closeOnContentClick: true,
    //     mainClass: 'mfp-img-mobile',
    //     image: {
    //         verticalFit: true
    //     }
    //
    // });

    //faqs

    $(".faqs dd").hide();
    $(document).on('click','.faqs dt', function () {
        $(this).next(".faqs dd").slideToggle(500);
        $(this).toggleClass("expanded");
    });

    if($('#volume').length > 0) {
        new Selectr('#volume', {
            searchable: false,
            width: 180
        });
    }

});
